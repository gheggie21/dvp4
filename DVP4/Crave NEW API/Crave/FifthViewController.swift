//
//  FifthViewController.swift
//  Crave
//
//  Created by Greg Heggie on 1/10/17.
//  Copyright © 2017 Greg Heggie. All rights reserved.
//

import UIKit

class FifthViewController: UIViewController, UISearchBarDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var recipeCollection: UICollectionView!
    @IBOutlet weak var mySearchBar: UISearchBar!
    
    var recipeArray = [recipe]()
    var filteredDesserts = [recipe]()
    var aDessert: Int!
    var recipeImages = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    // navigation color change
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(colorLiteralRed: 0.071, green: 0.278, blue: 0.286, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.init(colorLiteralRed: 0.851, green: 0.612, blue: 0.31, alpha: 1)]
        self.tabBarController?.tabBar.unselectedItemTintColor = UIColor.init(colorLiteralRed: 0.071, green: 0.278, blue: 0.286, alpha: 1)
    }
    
    // set this array to the main array
    override func viewWillAppear(_ animated: Bool) {
        recipeArray = dessertRecipes
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // keyboard dismisses when view is tapped
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //closes the keyboard when the view is clicked
        self.view.endEditing(true)
    }
    
    // when search field is tapped. it goes blank
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        mySearchBar.text = ""
    }
    
    // Search bar code
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        mySearchBar.resignFirstResponder()
        filteredDesserts = recipeArray
        
        let searchedRecipe = mySearchBar.text
        
        if searchedRecipe != "" {
            filteredDesserts = filteredDesserts.filter(
                { (rec) -> Bool in
                    return  (rec.recipeName.range(of: searchedRecipe!) != nil)
            })
            
        }
        recipeCollection.reloadData()
    }
    
    // Collection View Code
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredDesserts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recipeView2", for: indexPath) as! RecipeCell
        
        let url = "https://www.spoonacular.com/recipeImages/\(filteredDesserts[indexPath.row].recipeImage!)"
        cell.recipeName2.text = filteredDesserts[indexPath.row].recipeName
        loadImage(url, imageView: cell.recipeView2)
        
        return cell
    }
    
    // grabbing specific cell to send to RecipeView
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // grab cell to use for segue
        aDessert = indexPath.row
        performSegue(withIdentifier: "toRecipe", sender: indexPath)
    }
    
    // prepare RecipeView with the right information to be loaded
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toRecipe" {
            // grabs information out of certain cell to send to recipeView
            let rvc: RecipeViewController = segue.destination as! RecipeViewController
            rvc.theDessert = filteredDesserts[aDessert]
        }
    }
    
    // grab string of an image and put in views
    func loadImage(_ urlString:String, imageView: UIImageView)
    {
        let imageString: URL = URL(string: urlString)!
        
        let request: URLRequest = URLRequest(url: imageString)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            
            if (error == nil && data != nil)
            {
                func showPic()
                {
                    imageView.image =  UIImage(data: data!)
                }
                DispatchQueue.main.async(execute: showPic)
            }
        })
        task.resume()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
