//
//  ingredients.swift
//  Crave
//
//  Created by Greg Heggie on 1/26/17.
//  Copyright © 2017 Greg Heggie. All rights reserved.
//

import Foundation

class ingredientList {
    var name: String!
    var amount: Float!
    var unit: String!
    
    init(name: String, amount: Float, unit: String) {
        self.name = name
        self.amount = amount
        self.unit = unit
    }
}
