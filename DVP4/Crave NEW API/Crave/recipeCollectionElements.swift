//
//  recipeCollectionElements.swift
//  Crave
//
//  Created by Greg Heggie on 1/16/17.
//  Copyright © 2017 Greg Heggie. All rights reserved.
//

import UIKit

class RecipeCell: UICollectionViewCell {
    
    @IBOutlet weak var recipeView: UIImageView!
    @IBOutlet weak var recipeName: UILabel!
    
    @IBOutlet weak var recipeName2: UILabel!
    @IBOutlet weak var recipeView2: UIImageView!
    
    @IBOutlet weak var recipeName3: UILabel!
    @IBOutlet weak var recipeView3: UIImageView!
    
    @IBOutlet weak var recipeView4: UIImageView!
    @IBOutlet weak var recipeName4: UILabel!
    
}
