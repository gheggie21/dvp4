//
//  FourthViewController.swift
//  Crave
//
//  Created by Greg Heggie on 1/10/17.
//  Copyright © 2017 Greg Heggie. All rights reserved.
//

import UIKit

class FourthViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var aDessert: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }
    
    // navigation color change
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(colorLiteralRed: 0.071, green: 0.278, blue: 0.286, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.init(colorLiteralRed: 0.851, green: 0.612, blue: 0.31, alpha: 1)]
        self.tabBarController?.tabBar.unselectedItemTintColor = UIColor.init(colorLiteralRed: 0.071, green: 0.278, blue: 0.286, alpha: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // setting bookmarked recipes to collection view
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recipeView3", for: indexPath) as! RecipeCell
        
            var url = ""
            url = "https://www.spoonacular.com/recipeImages/\(savedRecipes[indexPath.row].recipeImage!)"
            cell.recipeName4.text = savedRecipes[indexPath.row].recipeName
            savedImages(url, imageView: cell.recipeView4)
        
        return cell

    }
    
    // number of cells
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return savedRecipes.count
    }
    
    // grab string of an image and put in views
    func savedImages(_ urlString:String, imageView: UIImageView)
    {
        let imageString: URL = URL(string: urlString)!
        
        let request: URLRequest = URLRequest(url: imageString)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            
            if (error == nil && data != nil)
            {
                func showPic()
                {
                    imageView.image =  UIImage(data: data!)
                }
                DispatchQueue.main.async(execute: showPic)
            }
        })
        task.resume()
    }
    
    // picking specific recipe to use in RecipeView
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        aDessert = indexPath.row
        performSegue(withIdentifier: "toRecipe", sender: indexPath)
    }
    
    // prepare RecipeView with the right information to be loaded
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toRecipe" {
            let rvc: RecipeViewController = segue.destination as! RecipeViewController
            rvc.theDessert = savedRecipes[aDessert]
        }
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
