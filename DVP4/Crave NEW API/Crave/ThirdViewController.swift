//
//  ThirdViewController.swift
//  Crave
//
//  Created by Greg Heggie on 1/10/17.
//  Copyright © 2017 Greg Heggie. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginBttn: UIButton!
    
    var myEmail = "crave@crave.com"
    var myPword = "crave1"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //closes the keyboard when the view is clicked
        self.view.endEditing(true)
    }
    
    // moves from one text field to the other. in order
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField
        {
        case emailField:
            passwordField.becomeFirstResponder()
        default:
            wrongInfo()
            textField.resignFirstResponder()
        }
        return true
    }
    // when textfield is clicked
    func textFieldDidBeginEditing(_ textField: UITextField) {
        view.frame.origin.y = -40
    }
    
    // when return/done is hit
    func textFieldDidEndEditing(_ textField: UITextField) {
        view.frame.origin.y = 0
    }
    
    // alert for when the wrong information was uploaded
    func wrongInfo() {
        // Alert message
        if emailField.text != myEmail || passwordField.text != myPword {
            let errorMessage = UIAlertController(title: "Incorret Information", message: "Information you entered was incorrect, please try again", preferredStyle: UIAlertControllerStyle.alert)
            let oK = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
            errorMessage.addAction(oK)
            self.present(errorMessage, animated: true , completion: nil)
            emailField.text = ""
            passwordField.text = ""
        } else {
            loginBttn.isEnabled = true
        }
    }
    
    // segues
    @IBAction func loginBttn(_ sender: UIButton) {
        performSegue(withIdentifier: "toCrave", sender: sender)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
