//
//  RecipeViewController.swift
//  Crave
//
//  Created by Greg Heggie on 1/10/17.
//  Copyright © 2017 Greg Heggie. All rights reserved.
//

import UIKit

class RecipeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var myScrollView: UIScrollView!
    @IBOutlet weak var dessertName: UILabel!
    @IBOutlet weak var ingredientTextView: UITextView!
    @IBOutlet weak var recipeView: UIImageView!
    @IBOutlet weak var simRecipes: UICollectionView!
    @IBOutlet weak var directionsTextView: UITextView!
    
    //variables
    var theDessert: recipe! = nil
    var similarRecipes = [recipe]()
    var step: Int = 0
    var number: Int = 1
    var steps = [String]()
    var ingredientArray =  [ingredientList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        myScrollView.scrollsToTop = true
        
        navigationItem.title = "Crave"
        
        dessertName.text = theDessert.recipeName!
        let url = "https://www.spoonacular.com/recipeImages/\(theDessert.recipeImage!)"
        loadImage(url)
        getSimilarRecipes()
        getTheIngredients()
        showIngredients()
        showDirections()
        }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let recipeID = "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/\(theDessert.recipeID!)/analyzedInstructions?stepBreakdown=true"
        
        if let url = URL(string: recipeID) {
            // create default configuration
            let config = URLSessionConfiguration.default
            
            let header = [
                "X-Mashape-Key":"VKPkfkudhbmsh8xTZNsZow1dkgpup19hVUAjsnrGoRftBMWuIk",
                "Accept":"application/json" ]
            
            config.httpAdditionalHeaders = header
            
            // create a session
            let session = URLSession(configuration: config)
            
            // create a data tasks with a valid url
            let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
                    
                // if error, print it
                if let actualError = error {
                    print("Data failed with error: \(actualError)")
                    return
                }
                    
                if let status = response as? HTTPURLResponse, let recipeData = data {
                        
                    // Status 200 is great and we can move on
                    if status.statusCode == 200 {
                            
                        DispatchQueue.main.async {
                            self.jsonParse(data: recipeData as NSData)
                        }
                    }
                }
            })
            task.resume()
        }
    }
    
    func showDirections() {
        while step < steps.count {
            directionsTextView.text.append("Step \(number): \(steps[step])\n\n")
            number += 1
            step += 1
        }
    }
    
    func showIngredients() {
        while step < ingredientArray.count {
            ingredientTextView.text.append("\(ingredientArray[step].amount!) \(ingredientArray[step].unit!) of \(ingredientArray[step].name!)\n")
            step += 1
        }
    }
    
    // navigation color changes
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(colorLiteralRed: 0.071, green: 0.278, blue: 0.286, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.init(colorLiteralRed: 0.851, green: 0.612, blue: 0.31, alpha: 1)]
        self.tabBarController?.tabBar.unselectedItemTintColor = UIColor.init(colorLiteralRed: 0.071, green: 0.278, blue: 0.286, alpha: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // json func to grab specific strings and print to the textView
    private func jsonParse(data: NSData) {
        do {
           let json = try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers)
            //grab all recipes
            let startArray = json as! [[String : Any]]
            for x in startArray {
                let recipeArray = x["steps"] as! [[String : Any]]
                for y in recipeArray {
                     steps.append(y["step"] as! String)
                }
            }
        }
        catch {
        print(error)
    }
}
    
    // grab string of an image and put in views
    func loadImage(_ urlString:String)
    {
        let imageString: URL = URL(string: urlString)!
        
        let request: URLRequest = URLRequest(url: imageString)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            
            if (error == nil && data != nil)
            {
                func showPic()
                {
                    self.recipeView.image = UIImage(data: data!)
                }
                DispatchQueue.main.async(execute: showPic)
            }
        })
        task.resume()
    }
    
    func getTheIngredients() {
        let urlString = "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/\(theDessert.recipeID!)/information?includeNutrition=false"
    
        if let url = URL(string: urlString) {
            
            let config = URLSessionConfiguration.default
            
            let header = [
                "X-Mashape-Key":"ZCqoyA3F3qmshaiiEoe3cBOBQRdmp1cZSttjsn0KuyLpz34PAX",
                "Accept":"application/json" ]
            
            config.httpAdditionalHeaders = header
            
            let urlSession = URLSession(configuration: config)
            
            let task = urlSession.dataTask(with: url, completionHandler: { (data, response, error) in
                
                // if error, print it
                if let actualError = error {
                    print("Data failed with error: \(actualError)")
                    return
                }
                
                if let status = response as? HTTPURLResponse, let recipeData = data {
                    
                    // Status 200 is great and we can move on
                    if status.statusCode == 200 {
                        
                        DispatchQueue.main.async {
                            self.ingreJsonParse(data: recipeData as NSData)
                            self.viewDidLoad()
                        }
                    }
                }
            })
            task.resume()
        }
    }
    
    private func ingreJsonParse(data: NSData) {
        do {
            let json = try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers)
            //grab all recipes
            if let recipeArray = json as? [String : Any] {
                let ingredients = recipeArray["extendedIngredients"] as! [[String : Any]]
                for x in ingredients {
                    ingredientArray.append(ingredientList(name: x["name"] as! String, amount: x["amount"] as! Float, unit: x["unit"] as! String))
                }
            }
        }
        catch {
            print(error)
        }
    }

    func getSimilarRecipes() {
        
        let urlString = "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/\(theDessert.recipeID!)/similar"
        
        if let url = URL(string: urlString) {
            
            let config = URLSessionConfiguration.default
            
            let header = [
                "X-Mashape-Key":"ZCqoyA3F3qmshaiiEoe3cBOBQRdmp1cZSttjsn0KuyLpz34PAX",
                "Accept":"application/json" ]
            
            config.httpAdditionalHeaders = header
            
            let urlSession = URLSession(configuration: config)
            
            let task = urlSession.dataTask(with: url, completionHandler: { (data, response, error) in
                
                // if error, print it
                if let actualError = error {
                    print("Data failed with error: \(actualError)")
                    return
                }
                
                if let status = response as? HTTPURLResponse, let recipeData = data {
                    
                    // Status 200 is great and we can move on
                    if status.statusCode == 200 {
                        
                        DispatchQueue.main.async {
                            self.simJsonParse(data: recipeData as NSData)
                            self.simRecipes.reloadData()
                        }
                    }
                }
            })
            task.resume()
        }
    }
    
    // json func to grab specific strings and add to array for similar recipes
    private func simJsonParse(data: NSData) {
        do {
            let json = try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers)
            //grab all recipes
            let recipeArray = json as! [[String : Any]]
            for x in recipeArray {
                if x["title"] == nil {
                    similarRecipes.append(recipe(recipeName: "Generic", recipeID: x["id"] as! Int, recipeImage: x["image"] as! String))
                } else if x["id"] == nil {
                    similarRecipes.append(recipe(recipeName: x["title"] as! String, recipeID: 506742, recipeImage: x["image"] as! String))
                } else if x["image"] == nil {
                    similarRecipes.append(recipe(recipeName: x["title"] as! String, recipeID: x["id"] as! Int, recipeImage: "https://spoonacular.com/application/frontend/images/badgeBest.jpg"))
                } else {
                    similarRecipes.append(recipe(recipeName: x["title"] as! String, recipeID: x["id"] as! Int, recipeImage: x["image"] as! String))
                }
            }
        }
        catch {
            print(error)
        }
    }

    // Similar Recipe Code
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "simRecipes", for: indexPath) as! RecipeCell

        var url = ""
        url = "https://www.spoonacular.com/recipeImages/\(self.similarRecipes[indexPath.row].recipeImage!)"
        cell.recipeName3.text = self.similarRecipes[indexPath.row].recipeName
        similarImages(url, imageView: cell.recipeView3)
        
        return cell
    }
    
    // number of similarRecipes
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return similarRecipes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // change recipe to this selected recipe.
        theDessert = similarRecipes[indexPath.row]
    }
    
    // grab string of an image and put in views
    func similarImages(_ urlString:String, imageView: UIImageView)
    {
        let imageString: URL = URL(string: urlString)!
        
        let request: URLRequest = URLRequest(url: imageString)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            
            if (error == nil && data != nil)
            {
                func showPic()
                {
                    imageView.image =  UIImage(data: data!)
                }
                DispatchQueue.main.async(execute: showPic)
            }
        })
        task.resume()
    }

    // when recipe is bookmarked. gives an alert
    @IBAction func bookmark(_ sender: UIButton) {
        let bookmarked = UIAlertController(title: "Bookmarked!", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        let oK = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
        bookmarked.addAction(oK)
        self.present(bookmarked, animated: true , completion: nil)
        
        savedRecipes.append(theDessert)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
