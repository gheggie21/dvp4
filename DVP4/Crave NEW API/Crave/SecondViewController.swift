//
//  SecondViewController.swift
//  Crave
//
//  Created by Greg Heggie on 1/10/17.
//  Copyright © 2017 Greg Heggie. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var recipeNameLabel: UILabel!
    @IBOutlet weak var recipeView: UIImageView!
    var ranRecipe: Int!
    var ranDesserts = [recipe]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        ranDesserts = dessertRecipes
        
        ranRecipe = Int(arc4random_uniform(UInt32(dessertRecipes.count)))
        recipeNameLabel.text = ranDesserts[ranRecipe].recipeName
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // create default configuration
        let config = URLSessionConfiguration.default
        
        // create a session
        let session = URLSession(configuration: config)
        
        // check for valid URL
        if let validURL = URL(string: "https://spoonacular.com/recipeImages/\(ranDesserts[ranRecipe].recipeImage!)") {
                
                // create a data tasks with a valid url
                let task = session.dataTask(with: validURL, completionHandler: { (data, response, error) in
                    
                    // if error, print it
                    if let actualError = error {
                        print("Data failed with error: \(actualError)")
                        return
                    }
                    
                    if let status = response as? HTTPURLResponse, let data = data {
                        
                        let recipePic = UIImage(data: data)
                        // Status 200 is great and we can move on
                        if status.statusCode == 200 {
                            
                            DispatchQueue.main.async {
                                self.recipeView.image = recipePic
                                self.reloadInputViews()
                            }
                        }
                    }
                })
                task.resume()
            }
    }
    // changing nav bar colors
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(colorLiteralRed: 0.071, green: 0.278, blue: 0.286, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.init(colorLiteralRed: 0.851, green: 0.612, blue: 0.31, alpha: 1)]
        self.tabBarController?.tabBar.unselectedItemTintColor = UIColor.init(colorLiteralRed: 0.071, green: 0.278, blue: 0.286, alpha: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // random generate
    @IBAction func randomRecipe(_ sender: UIButton) {
        ranRecipe = Int(arc4random_uniform(UInt32(dessertRecipes.count)))
        recipeNameLabel.text = ranDesserts[ranRecipe].recipeName
        self.viewWillAppear(true)
        
    }
    
    // prepare Recipe View with the right information to be loaded
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toRecipe" {
            let rvc: RecipeViewController = segue.destination as! RecipeViewController
            rvc.theDessert = ranDesserts[ranRecipe]
        }
    }
}

