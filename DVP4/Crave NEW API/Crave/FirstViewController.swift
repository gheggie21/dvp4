//
//  FirstViewController.swift
//  Crave
//
//  Created by Greg Heggie on 1/10/17.
//  Copyright © 2017 Greg Heggie. All rights reserved.
//

import UIKit
var dessertRecipes =  [recipe]()
var savedRecipes = [recipe]()

class FirstViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITabBarDelegate{
    
    @IBOutlet weak var recipeCollection: UICollectionView!
    
    //custom object array for recipes
    var aDessert: Int = 0
     
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    // grab recipes from api and add to array
    override func viewWillAppear(_ animated: Bool) {
        
        let urlString = "https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/search?instructionsRequired=true&limitLicense=false&number=100&offset=35&type=dessert"
        
        if let url = URL(string: urlString) {
            
            let config = URLSessionConfiguration.default
            
            let header = [
                "X-Mashape-Key":"VKPkfkudhbmsh8xTZNsZow1dkgpup19hVUAjsnrGoRftBMWuIk",
                "Accept":"application/json" ]
            
            config.httpAdditionalHeaders = header
            
            let urlSession = URLSession(configuration: config)
            
            let task = urlSession.dataTask(with: url, completionHandler: { (data, response, error) in
                
                // if error, print it
                if let actualError = error {
                    print("Data failed with error: \(actualError)")
                    return
                }
                
                if let status = response as? HTTPURLResponse, let recipeData = data {
                    
                    // Status 200 is great and we can move on
                    if status.statusCode == 200 {
                        
                        DispatchQueue.main.async {
                            self.jsonParse(data: recipeData as NSData)
                            self.recipeCollection.reloadData()
                        }
                    }
                }
            })
            task.resume()
        }
    }
    
    // navigation and tabBar color changes
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(colorLiteralRed: 0.071, green: 0.278, blue: 0.286, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.init(colorLiteralRed: 0.851, green: 0.612, blue: 0.31, alpha: 1)]
        self.tabBarController?.tabBar.unselectedItemTintColor = UIColor.init(colorLiteralRed: 0.071, green: 0.278, blue: 0.286, alpha: 1)
    }
    
    // json func to grab specific strings and add to array
    private func jsonParse(data: NSData) {
        do {
            let json = try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers)
            //grab all recipes
            if let recipeArray = json as? [String : Any] {
                // put the recipes in an array
                if let matchArray = recipeArray["results"] as? [[String : Any]] {
                    for x in matchArray {
                        dessertRecipes.append(recipe(recipeName: x["title"] as! String, recipeID: x["id"] as! Int, recipeImage: x["image"] as! String))
                    }
                }
            }
        }
        catch {
            print(error)
        }
    }
    // configure each cell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recipeView", for: indexPath) as! RecipeCell
    
        var url = ""
        url = "https://www.spoonacular.com/recipeImages/\(dessertRecipes[indexPath.row].recipeImage!)"
        cell.recipeName.text = dessertRecipes[indexPath.row].recipeName
        self.loadImage(url, imageView: cell.recipeView)
        
        return cell
    }
    
    // grab string of an image and put in views
    func loadImage(_ urlString:String, imageView: UIImageView)
    {
        let imageString = URL(string: urlString)!
        
        let request: URLRequest = URLRequest(url: imageString)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            
            if (error == nil && data != nil)
            {
                // adding image to imageView
                func showPic()
                {
                    imageView.image =  UIImage(data: data!)
                }
                DispatchQueue.main.async(execute: showPic)
            }
        })
        task.resume()
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // number of recipes showing
        return dessertRecipes.count
    }
    
    //grab specific recipe
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        aDessert = indexPath.row
        performSegue(withIdentifier: "toRecipe", sender: indexPath)
    }
    
    // prepare RecipeView with the right information to be loaded
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toRecipe" {
            let rvc: RecipeViewController = segue.destination as! RecipeViewController
            rvc.theDessert = dessertRecipes[aDessert]
        }
    }
}

