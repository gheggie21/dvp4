//
//  Recipes.swift
//  Crave
//
//  Created by Greg Heggie on 1/16/17.
//  Copyright © 2017 Greg Heggie. All rights reserved.
//

import Foundation

class recipe {
    var recipeName: String!
    var recipeID: Int!
    var recipeImage: String!
    
    init(recipeName: String, recipeID: Int, recipeImage: String) {
        self.recipeName = recipeName
        self.recipeID = recipeID
        self.recipeImage = recipeImage
    }
    
}
