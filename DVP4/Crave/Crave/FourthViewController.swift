//
//  FourthViewController.swift
//  Crave
//
//  Created by Greg Heggie on 1/10/17.
//  Copyright © 2017 Greg Heggie. All rights reserved.
//

import UIKit

class FourthViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // navigation color change
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(colorLiteralRed: 0.071, green: 0.278, blue: 0.286, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.init(colorLiteralRed: 0.851, green: 0.612, blue: 0.31, alpha: 1)]
        self.tabBarController?.tabBar.unselectedItemTintColor = UIColor.init(colorLiteralRed: 0.071, green: 0.278, blue: 0.286, alpha: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //FIX: this will change was API access is gained
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recipeView3", for: indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // FIX: this will change was API access is gained
        return 4
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
