//
//  FifthViewController.swift
//  Crave
//
//  Created by Greg Heggie on 1/10/17.
//  Copyright © 2017 Greg Heggie. All rights reserved.
//

import UIKit

class FifthViewController: UIViewController, UISearchBarDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var recipeCollection: UICollectionView!
    @IBOutlet weak var mySearchBar: UISearchBar!
    
    var recipeArray = [recipe]()
    var filteredDesserts = [recipe]()
    var aDessert: Int!
    var recipeImages = [UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    // navigation color change
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(colorLiteralRed: 0.071, green: 0.278, blue: 0.286, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.init(colorLiteralRed: 0.851, green: 0.612, blue: 0.31, alpha: 1)]
        self.tabBarController?.tabBar.unselectedItemTintColor = UIColor.init(colorLiteralRed: 0.071, green: 0.278, blue: 0.286, alpha: 1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        recipeArray = dessertRecipes
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //closes the keyboard when the view is clicked
        self.view.endEditing(true)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        mySearchBar.text = ""
    }
    
    // Search bar code
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        mySearchBar.resignFirstResponder()
        filteredDesserts = recipeArray
        
        let searchedRecipe = mySearchBar.text
        
        if searchedRecipe != "" {
            filteredDesserts = filteredDesserts.filter(
                { (rec) -> Bool in
                    return  (rec.recipeName.range(of: searchedRecipe!) != nil)
            })
            inputImages()
            recipeCollection.reloadData()
        }
    }
    
    // Collection View Code
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredDesserts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recipeView2", for: indexPath) as! RecipeCell
        
        for x in indexPath {
            cell.recipeName2.text = filteredDesserts[x].recipeName
            cell.recipeView2.image = recipeImages[x]
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // grab cell to use for segue
        aDessert = indexPath.row
        performSegue(withIdentifier: "toRecipe", sender: indexPath)
    }
    
    // prepare RecipeView with the right information to be loaded
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toRecipe" {
            // grabs information out of certain cell to send to recipeView
            let rvc: RecipeViewController = segue.destination as! RecipeViewController
            rvc.theDessert = filteredDesserts[aDessert]
        }
    }
    
    func inputImages() {        
        for x in filteredDesserts {
            for y in x.recipeImage {
                // create default configuration
                let config = URLSessionConfiguration.default
                
                // create a session
                let session = URLSession(configuration: config)
                
                // grab images from URL only if search has results
                if let validURL = URL(string: y) {
                    
                    // create a data tasks with a valid url
                    let task = session.dataTask(with: validURL, completionHandler: { (data, response, error) in
                        
                        // if error, print it
                        if let actualError = error {
                            print("Data failed with error: \(actualError)")
                            return
                        }
                        
                        if let status = response as? HTTPURLResponse, let data = data {
                            
                            let recipePic = UIImage(data: data)
                            // Status 200 is great and we can move on
                            if status.statusCode == 200 {
                                self.recipeImages.append(recipePic!)
                            }
                        }
                    })
                    task.resume()
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
