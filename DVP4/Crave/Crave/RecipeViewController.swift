//
//  RecipeViewController.swift
//  Crave
//
//  Created by Greg Heggie on 1/10/17.
//  Copyright © 2017 Greg Heggie. All rights reserved.
//

import UIKit

class RecipeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var dessertName: UILabel!
    @IBOutlet weak var ingredientTextView: UITextView!
    @IBOutlet weak var directionTextView: UITextView!
    @IBOutlet weak var recipeView: UIImageView!
    
    //variables
    var theDessert: recipe! = nil
    var simRecipes = [recipe]()
    var recipeSite = ""
    var recipePic = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        dessertName.text = theDessert.recipeName
        
        for x in theDessert.ingredients {
            ingredientTextView.text.append("• \(x.uppercased())\n")
        }
        navigationItem.title = "Crave"
        
        if recipePic != "" {
           getPic()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let recipeID = "https://api.yummly.com/v1/api/recipe/\(theDessert.recipeID!)?_app_id=3aed8d59&_app_key=edb996b85995a10433240e8c83d58c51"
        
        // create default configuration
        let config = URLSessionConfiguration.default
        
        // create a session
        let session = URLSession(configuration: config)
        
        // check for valid URL
        if let validURL = URL(string: recipeID) {
            
            // create a data tasks with a valid url
            let task = session.dataTask(with: validURL, completionHandler: { (data, response, error) in
                
                // if error, print it
                if let actualError = error {
                    print("Data failed with error: \(actualError)")
                    return
                }
                
                if let status = response as? HTTPURLResponse, let recipeData = data {
                    
                    // Status 200 is great and we can move on
                    if status.statusCode == 200 {
                        
                        DispatchQueue.main.async {
                            self.jsonParse(data: recipeData as NSData)
                            self.viewDidLoad()
                        }
                    }
                }
            })
            task.resume()
        }
    }
    
    func getPic() {
        // create default configuration
        let config = URLSessionConfiguration.default
        
        // create a session
        let session = URLSession(configuration: config)
        
        if let validURL = URL(string: recipePic) {
            
            // create a data tasks with a valid url
            let task = session.dataTask(with: validURL, completionHandler: { (data, response, error) in
                
                // if error, print it
                if let actualError = error {
                    print("Data failed with error: \(actualError)")
                    return
                }
                
                if let status = response as? HTTPURLResponse, let data = data {
                    
                    let recipePix = UIImage(data: data)
                    // Status 200 is great and we can move on
                    if status.statusCode == 200 {
                        
                        DispatchQueue.main.async {
                            self.recipeView.image = recipePix
                        }
                    }
                }
            })
            task.resume()
        }
    }
    
    // navigation color changes
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(colorLiteralRed: 0.071, green: 0.278, blue: 0.286, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.init(colorLiteralRed: 0.851, green: 0.612, blue: 0.31, alpha: 1)]
        self.tabBarController?.tabBar.unselectedItemTintColor = UIColor.init(colorLiteralRed: 0.071, green: 0.278, blue: 0.286, alpha: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // json func to grab specific strings and print to the textView
    private func jsonParse(data: NSData) {
        do {
           let json = try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers)
            //grab all recipes
            if let recipeArray = json as? [String : Any] {
                if let sourceRecipe = recipeArray["source"] as? [String : Any] {
                    if let theRecipe = sourceRecipe["sourceRecipeUrl"] as? String? {
                        recipeSite = theRecipe!
                    }
                }
                if let recipeImages = recipeArray["images"] as? [[String: Any]] {
                    for x in recipeImages {
                        if let recipeImage = x["hostedLargeUrl"] as? String? {
                            recipePic = recipeImage!
                        }
                    }
                }
            }
        }
        catch {
            print(error)
        }
    }

    // Similar Recipe Code
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //FIX: this will change was API access is gained
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "simRecipes", for: indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return simRecipes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // change recipe to this selected recipe.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
