//
//  FirstViewController.swift
//  Crave
//
//  Created by Greg Heggie on 1/10/17.
//  Copyright © 2017 Greg Heggie. All rights reserved.
//

import UIKit
var dessertRecipes =  [recipe]()

class FirstViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITabBarDelegate{
    
    @IBOutlet weak var recipeCollection: UICollectionView!
    
    //custom object array for recipes
    var aDessert: Int = 0
    var recipePhotos = [UIImage]()
    var recipePics = [String]()
    
     
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if dessertRecipes.count != 0 {
            inputImages()
        }
        if recipePics != [] {
            addPics()
            recipeCollection.reloadData()
        }    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // configure a session for url and grab json data
        let config = URLSessionConfiguration.default
        
        let session = URLSession(configuration: config)
        
        if let validURL = URL(string: "https://api.yummly.com/v1/api/recipes?_app_id=3aed8d59&_app_key=edb996b85995a10433240e8c83d58c51&q=desserts&maxResult=25&start=10") {
            
            let task = session.dataTask(with: validURL, completionHandler: { (data, response, error) in
                
                // if error, print it
                if let actualError = error {
                    print("Data failed with error: \(actualError)")
                    return
                }
                
                if let status = response as? HTTPURLResponse, let data = data {
                    
                    // Status 200 is great and we can move on
                    if status.statusCode == 200 {
                        self.jsonParse(data: data as NSData)
                        self.viewDidLoad()
                    }
                }
            })
            task.resume()
        }
    }
    
    // navigation and tabBar color changes
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = UIColor.init(colorLiteralRed: 0.071, green: 0.278, blue: 0.286, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.init(colorLiteralRed: 0.851, green: 0.612, blue: 0.31, alpha: 1)]
        self.tabBarController?.tabBar.unselectedItemTintColor = UIColor.init(colorLiteralRed: 0.071, green: 0.278, blue: 0.286, alpha: 1)
    }
    
    
    // json func to grab specific strings and add to array
    private func jsonParse(data: NSData) {
        do {
            let json = try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers)
            //grab all recipes
            if let recipeArray = json as? [String : Any] {
                // put the recipes in an array
                if let matchArray = recipeArray["matches"] as? [[String : Any]] {
                    for x in matchArray {
                        dessertRecipes.append(recipe(recipeName: x["recipeName"] as! String, recipeID: x["id"] as! String, recipeImage: x["smallImageUrls"] as! [String], ingredients: x["ingredients"] as! [String]))
                    }
                }
            }
        }
        catch {
            print(error)
        }
    }
    
    func inputImages() {
        for x in dessertRecipes {
            
            let recipeID = "https://api.yummly.com/v1/api/recipe/\(x.recipeID!)?_app_id=3aed8d59&_app_key=edb996b85995a10433240e8c83d58c51"
            
            // create default configuration
            let config = URLSessionConfiguration.default
            
            // create a session
            let session = URLSession(configuration: config)
            
            // check for valid URL
            if let validURL = URL(string: recipeID) {
                
                // create a data tasks with a valid url
                let task = session.dataTask(with: validURL, completionHandler: { (data, response, error) in
                    
                    // if error, print it
                    if let actualError = error {
                        print("Data failed with error: \(actualError)")
                        return
                    }
                    
                    if let status = response as? HTTPURLResponse, let recipeData = data {
                        
                        // Status 200 is great and we can move on
                        if status.statusCode == 200 {
                            self.newJsonParse(data: recipeData as NSData)
                        }
                    }
                })
                task.resume()
            }
        }
    }
    
    // json func to grab specific image strings
    private func newJsonParse(data: NSData) {
        do {
            let json = try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers)
            //grab all recipes
            if let recipeArray = json as? [String : Any] {
                if let recipeImages = recipeArray["images"] as? [[String: Any]] {
                    for x in recipeImages {
                        if let recipeImage = x["hostedMediumUrl"] as? String? {
                            recipePics.append(recipeImage!)
                        }
                    }
                }
            }
        }
        catch {
            print(error)
        }
    }

    // turn strings into images for collection view
    func addPics() {
        for x in recipePics {
            // create default configuration
            let config = URLSessionConfiguration.default
            
            // create a session
            let session = URLSession(configuration: config)
            
            if let validURL = URL(string: x) {
                
                // create a data tasks with a valid url
                let task = session.dataTask(with: validURL, completionHandler: { (data, response, error) in
                    
                    // if error, print it
                    if let actualError = error {
                        print("Data failed with error: \(actualError)")
                        return
                    }
                    
                    if let status = response as? HTTPURLResponse, let data = data {
                        
                        let recipePix = UIImage(data: data)
                        // Status 200 is great and we can move on
                        if status.statusCode == 200 {
                            self.recipePhotos.append(recipePix!)
                        } 
                    }
                })
                task.resume()
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recipeView", for: indexPath) as! RecipeCell
        
        for x in indexPath {
            cell.recipeName.text = dessertRecipes[x].recipeName
            if recipePhotos != [] {
                cell.recipeView.image = recipePhotos[x]
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // number of recipes showing
        return dessertRecipes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        aDessert = indexPath.row
        performSegue(withIdentifier: "toRecipe", sender: indexPath)
    }
    
    // prepare RecipeView with the right information to be loaded
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toRecipe" {
            let rvc: RecipeViewController = segue.destination as! RecipeViewController
            rvc.theDessert = dessertRecipes[aDessert]
        }
    }
}

